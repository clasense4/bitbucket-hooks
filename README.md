# Bitbucket Hooks
Ini adalah test repository untuk menggunakan fitur di bitbucket, yaitu
[**`hooks POST Management`**](https://confluence.atlassian.com/display/BITBUCKET/POST+hook+management).

Fitur ini bisa dipergunakan untuk mempermudah dalam proses deployment sebuah project pada sebuah server production, khususnya linux.

## Persyaratan

1. 1 project di bitbucket
2. 1 server production
3. Telah melakukan setting `Deployment Keys` pada repository / bisa pull pada repository tersebut

---